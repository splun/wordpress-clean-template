$(document).ready(function(){

  $("#slider").owlCarousel({
  	items:1,
  	autoplay:true,
  	autoplayTimeout: 10000,
  	loop:true,
  	nav:false,
    margin:0,
    dots:false
  });

  $("#articles").owlCarousel({
  	items:1,
  	autoplay:true,
  	autoplayTimeout: 10000,
  	loop:true,
  	nav:true,
    margin:0,
    dots:false,
    navText:["<img src='assets/images/angle-left.png'>","<img src='assets/images/angle-right.png'>"]
  });

});